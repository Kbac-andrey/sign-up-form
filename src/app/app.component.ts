import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpComponent } from './sign-up/sign-up.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, SignUpComponent],
  templateUrl: './app.component.html',
})
export class AppComponent {
}
