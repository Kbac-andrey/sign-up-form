import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  static emailValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const email: string = control.value;
      const emailRegex: RegExp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
      const isValidFormat: boolean = emailRegex.test(email);
      return isValidFormat ? null : { invalidEmailFormat: true };
    };
  }

  static passwordMatchValidator(control: FormGroup): ValidationErrors | null {
    const password: AbstractControl<string> | null = control.get('password');
    const confirmPassword: AbstractControl<string> | null = control.get('confirmPassword');

    if (!password || !confirmPassword) {
      return null;
    }

    return password.value === confirmPassword.value ? null : { passwordMismatch: true };
  }

  static passwordValidatorLowerAndUpper(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value: string = control.value;
      const hasLowercase: boolean = /[a-z]/.test(value);
      const hasUppercase: boolean = /[A-Z]/.test(value);

      return hasLowercase && hasUppercase ? null : { passwordLowerUpperError: true };
    };
  }

  static passwordValidatorContainNameLastName(firstNameControl: any, lastNameControl: any): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value: string = control?.value;
      const firstName: string = firstNameControl?.value;
      const lastName: string = lastNameControl?.value;
      const containsFirstName: boolean = firstName?.length ? value.toLowerCase().includes(firstName.toLowerCase()) : false;
      const containsLastName: boolean = lastName?.length ? value.toLowerCase().includes(lastName.toLowerCase()) : false;

      return containsFirstName || containsLastName ? { passwordContainsNameLastNameError: true } : null;
    };
  }
}
