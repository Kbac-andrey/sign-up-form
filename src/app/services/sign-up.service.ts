import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ISignUpPayload } from '../types/signUp';

@Injectable({
  providedIn: 'root'
})
export class SignUpService {

  constructor(private _http: HttpClient) { }

  signUp(payload : ISignUpPayload): Observable<ISignUpPayload> {
    return this._http.post<ISignUpPayload>(`https://demo-api.now.sh/users`, payload);
  }
}
