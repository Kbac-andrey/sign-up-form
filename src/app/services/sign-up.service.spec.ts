import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SignUpService } from './sign-up.service';
import { ISignUpPayload } from '../types/signUp';

describe('SignUpService', () => {
  let service: SignUpService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SignUpService]
    });

    service = TestBed.inject(SignUpService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send a POST request with the correct payload', () => {
    const mockPayload: ISignUpPayload = {
      email: 'marchenko@gmail.com',
      firstName: 'Andrii',
      lastName: 'Marchenko',
      password: 'test'
    };

    service.signUp(mockPayload).subscribe();

    const req = httpMock.expectOne('https://demo-api.now.sh/users');
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual(mockPayload);

    req.flush({
      email: 'marchenko@gmail.com',
      firstName: 'Andrii',
      lastName: 'Marchenko',
      _id: "27ef6e8b-44d1-4a2f-9dfb-e29a29855fdb"
    });
  });

  it('should return an Observable<ISignUpPayload>', () => {
    const mockPayload: ISignUpPayload = {
      email: 'marchenko@gmail.com',
      firstName: 'Andrii',
      lastName: 'Marchenko',
      password: 'test'
    };

    service.signUp(mockPayload).subscribe(response => {
      expect(response).toEqual(mockPayload);
    });

    const req = httpMock.expectOne('https://demo-api.now.sh/users');
    req.flush(mockPayload);
  });

});
