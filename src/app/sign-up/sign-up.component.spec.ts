import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SignUpComponent } from './sign-up.component';
import { SignUpService } from '../services/sign-up.service';

describe('SignUpComponent', () => {
  let component: SignUpComponent;
  let fixture: ComponentFixture<SignUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, HttpClientModule],
      providers: [SignUpService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form correctly', () => {
    expect(component.signUpForm).toBeDefined();
    expect(component.signUpForm.get('firstName')).toBeDefined();
    expect(component.signUpForm.get('lastName')).toBeDefined();
    expect(component.signUpForm.get('email')).toBeDefined();
    expect(component.signUpForm.get('passwordGroup')).toBeDefined();
  });

  it('should mark form as touched on submit when form is invalid', () => {
    spyOn(component.signUpForm, 'markAllAsTouched');
    component.onSubmit();
    expect(component.signUpForm.markAllAsTouched).toHaveBeenCalled();
  });

  it('should display error messages for invalid form fields', () => {
    component.signUpForm.setValue({
      firstName: '',
      lastName: '',
      email: 'invalid-email',
      passwordGroup: {
        password: 'password',
        confirmPassword: 'password',
      },
    });

    component.onSubmit();

    expect(component.signUpForm.get('firstName')?.hasError('required')).toBeTrue();
    expect(component.signUpForm.get('lastName')?.hasError('required')).toBeTrue();
    expect(component.signUpForm.get('email')?.hasError('email')).toBeTrue();
  });

  it('should disable the submit button when the form is invalid', () => {
    component.signUpForm.setValue({
      firstName: '',
      lastName: '',
      email: 'invalid-email',
      passwordGroup: {
        password: 'password',
        confirmPassword: 'password',
      },
    });

    fixture.detectChanges();

    const submitButton = fixture.nativeElement.querySelector('.sign-up-container__action');
    expect(submitButton.disabled).toBeTrue();
  });
});
