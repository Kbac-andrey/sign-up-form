import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ValidationService } from '../services/validation.service'
import { SignUpService } from '../services/sign-up.service';
import { HttpClientModule } from '@angular/common/http';
import { ISignUpPayload } from '../types/signUp';

@Component({
  selector: 'app-sign-up',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, HttpClientModule],
  providers: [SignUpService],
  templateUrl: './sign-up.component.html',
  styleUrl: './sign-up.component.scss'
})
export class SignUpComponent implements OnInit {
  public signUpForm!: FormGroup;

  public constructor( private _formBuilder: FormBuilder, private _signUpService: SignUpService) {
  }

  public ngOnInit(): void {
    this.signUpForm =  this._formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email,ValidationService.emailValidator()]],
    });

    this.signUpForm.addControl('passwordGroup', this._formBuilder.group({
      password: [
        '',
        [
          Validators.minLength(8),
          Validators.required,
          ValidationService.passwordValidatorContainNameLastName(
            this.signUpForm.get('firstName:'),
            this.signUpForm.get('lastName')
          ),
          ValidationService.passwordValidatorLowerAndUpper()
        ]
      ],
      confirmPassword: ['', Validators.required],

    }, { validator: ValidationService.passwordMatchValidator }));
  }

  public onSubmit(): void {
    if (this.signUpForm.invalid) {
      this.signUpForm.markAllAsTouched();
      return;
    }

    const {firstName , lastName, email, passwordGroup} = this.signUpForm.value;
    const payload: ISignUpPayload = {
      firstName,lastName, email, password: passwordGroup.password
    }

    this._signUpService.signUp(payload).subscribe(
      (response) => {
        console.log('POST request successful', response);
      },
      (error) => {
        console.error('Error submitting the form', error);
      }
    );
  }
}
